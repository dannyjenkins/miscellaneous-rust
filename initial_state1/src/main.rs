use itertools::Itertools;

struct LFSR {
    pub cells: Vec<u32>,
    pub taps: Vec<u32>,
    pub original_cells: Vec<u32>,
}

impl LFSR {
    pub fn new(cell_values: Vec<u32>, tap_values: Vec<u32>) -> LFSR {
        if cell_values.len() < tap_values.len() {
            panic!("The number of taps cannot be greater than the length of the register");
        }

        for value in &cell_values {
            if *value != 1 && *value != 0 {
                panic!(
                    "The register (cell_values) cannot contain values which are not either \
                        0 or 1"
                )
            }
        }

        let new_lfsr = LFSR {
            cells: cell_values.clone(),
            taps: tap_values.clone(),
            original_cells: cell_values.clone(),
        };
        new_lfsr
    }

    fn reset_cells(&mut self) {
        self.cells = self.original_cells.clone();
    }

    fn clock(&mut self) {
        let mut prepend_value: u32 = 0;
        for n in 0..self.taps.len() {
            prepend_value ^= self.cells[self.cells.len() - ((self.taps[n] + 1) as usize)];
        }
        for n in (1..self.cells.len()).rev() {
            self.cells[n] = self.cells[n - 1];
        }
        self.cells[0] = prepend_value;
    }

    fn generate_output_list(&mut self, length: u32) -> Vec<u32> {
        let mut return_vec: Vec<u32> = vec![];
        for _x in 0..length {
            return_vec.push(self.get_last());
            self.clock();
        }
        return_vec
    }

    fn get_value(&self, index: usize) -> u32 {
        self.cells[index]
    }

    fn get_last(&self) -> u32 {
        self.get_value(self.cells.len() - 1)
    }
}

fn probability_same(l1: Vec<u32>, l2: &Vec<u32>) -> f32 {
    let mut number_of_same: u32 = 0;
    for x in 0..l1.len() {
        if l1[x] == l2[x] {
            number_of_same += 1;
        }
    }
    (number_of_same as f32) / (l1.len() as f32)
}

fn main() {
    let key_stream: Vec<u32> = vec![
        0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0,
        1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1,
        0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1,
        1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0,
        0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1,
        0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1,
        1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0,
    ];

    let bitgroups = (0..11).map(|_i| (0)..(2)).multi_cartesian_product();

    for value in bitgroups {
        let mut x1: LFSR = LFSR::new(value.clone(), vec![8, 5, 3, 0]);
        let output_list: Vec<u32> = x1.generate_output_list(200);
        let prob: f32 = probability_same(output_list, &key_stream);
        println!("{prob} {:?}", value);
    }
}
